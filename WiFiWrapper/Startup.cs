﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WiFiWrapper.Model;
using WiFiWrapper.View;
using WiFiWrapper.ViewModel;

namespace WiFiWrapper
{
    public class Startup
    {
        [STAThread]
        static void Main()
        {
            ///ioc initialization
            IUnityContainer container = new UnityContainer();
            container.RegisterType<IApplication, Application>();
            container.RegisterType<IWiFiWrapperView, WiFiWrapperView>();
            container.RegisterType<IWiFiWrapperViewModel, WiFiWrapperViewModel>();
            container.RegisterType<IWifiWrapper, Model.WiFiWrapper>();
            container.RegisterType<IWifiState, WifiState>();

            ///running app
            var app = container.Resolve<IApplication>();
            app.Run();
        }
    }
}
