﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WiFiWrapper
{
    /// <summary>
    /// application interface
    /// </summary>
    public interface IApplication
    {
        int Run();///method to run application
    }
}
