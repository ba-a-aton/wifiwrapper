﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WiFiWrapper.View;

namespace WiFiWrapper
{
    public class Application:System.Windows.Application,IApplication
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="view">main view of application</param>
        public Application(IWiFiWrapperView view)
        {
            view.Show();///show window
        }
    }
}
