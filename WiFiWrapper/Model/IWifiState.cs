﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WiFiWrapper.Model
{
    /// <summary>
    /// interface for wifi state
    /// </summary>
    public interface IWifiState
    {
        bool IsInsecure { get; set; }///is connection secured
        int InterfaceIndex { get; set; }///index of interface
        string InterfaceName { get; set; }//name of interface
        string SSID { get; set; }//wifi ssid
    }
}
