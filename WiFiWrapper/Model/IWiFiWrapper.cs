﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WiFiWrapper.Model
{
    public delegate void WifiNotification(IWifiState state);

    /// <summary>
    /// interface of wrapper
    /// </summary>
    public interface IWifiWrapper
    {
        event WifiNotification WifiStateChanged;//event
        WifiState CurrentState { get; }//current state of interface
        void Initialize();//init method
        void Shutdown();//shutdown method
    }
}
