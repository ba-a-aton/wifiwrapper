﻿using NativeWifi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace WiFiWrapper.Model
{
    public class WiFiWrapper : IWifiWrapper
    {
        WlanClient client = new WlanClient();
        WifiState currentState = new WifiState();

        public WifiState CurrentState => currentState;

        WifiNotification changed;

        /// <summary>
        /// subscribing to notification
        /// </summary>
        public event WifiNotification WifiStateChanged
        {
            add => changed += value; 
            remove => changed -= value; 
        }

        /// <summary>
        /// constructor
        /// </summary>
        public WiFiWrapper() { }

        /// <summary>
        /// start library initialization
        /// </summary>
        public void Initialize()
        {
            manageState();
            Timer timer = new Timer(1000);
            timer.Elapsed += async (sender, e) => await getState();
            timer.Start();
        }

        public void Shutdown() { }

        private Task getState()
        {
            return Task.Run(()=>manageState());
        }

        private void manageState()
        {
            if (client.Interfaces != null && client.Interfaces.Count() != 0)
            {
                int index = 0;
                foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
                {
                    if (wlanIface.InterfaceState == Wlan.WlanInterfaceState.Connected)
                    {
                        currentState.SSID = wlanIface.CurrentConnection.profileName;
                        currentState.InterfaceIndex = index;
                        currentState.InterfaceName = wlanIface.InterfaceName;
                        currentState.IsInsecure = !wlanIface.CurrentConnection.wlanSecurityAttributes.securityEnabled;
                        break;
                    }
                    else
                    {
                        setNoWiFiState();
                        index++;
                    }
                }
            }
            else setNoWlanState();
            changed?.BeginInvoke(currentState, null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        private void setNoWlanState()
        {
            currentState.SSID = "null";
            currentState.IsInsecure = true;
            currentState.InterfaceIndex = -1;
            currentState.InterfaceName = "null";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="interfaceIndex"></param>
        /// <param name="interfaceName"></param>
        private void setNoWiFiState()
        {
            currentState.InterfaceIndex = -2;
            currentState.SSID = "null";
            currentState.IsInsecure = false;
            currentState.InterfaceName = "null";
        }

     }
}
