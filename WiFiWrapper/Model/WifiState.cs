﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WiFiWrapper.Model
{
    public class WifiState : IWifiState
    {
        public bool IsInsecure { get; set; }//auto isinsecure implementation

        public int InterfaceIndex { get; set; }//auto interface index implementation

        public string InterfaceName { get; set; }//auto interface name implementation

        public string SSID { get; set; }//auto ssid implementation
    }
}
