﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WiFiWrapper.Model;

namespace WiFiWrapper.ViewModel
{
    public class WiFiWrapperViewModel:WiFiWrapperViewModelBase, IWiFiWrapperViewModel
    {
        bool isInsecure = true;
        int interfaceIndex;
        string interfaceName;
        string sSID;
        bool hasNoWLAN = false;
        bool hasNoWiFiConnection = false;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="model">IWiFiWrapperModel</param>
        public WiFiWrapperViewModel(IWifiWrapper model)
        {
            model.WifiStateChanged += Model_WifiStateChanged;//subscribing event
            model.Initialize();///model start initialization
        }

        /// <summary>
        /// method for state changed
        /// </summary>
        /// <param name="state">new state of wlan</param>
        private void Model_WifiStateChanged(IWifiState state)
        {
            IsInsecure = state.IsInsecure;
            InterfaceIndex = state.InterfaceIndex;
            InterfaceName = state.InterfaceName;
            SSID = state.SSID;
            switch (interfaceIndex)
            {
                case -1:
                    HasNoWLAN = true;
                    HasNoWiFiConnection = false;
                    break;
                case -2:
                    HasNoWiFiConnection = true;
                    HasNoWLAN = false;
                    break;
                default:
                    HasNoWLAN = false;
                    HasNoWiFiConnection = false;
                    break;
            }
        }


        #region properties
        /// <summary>
        /// is insecure connection
        /// </summary>
        public bool IsInsecure 
        {
            get => isInsecure;
            set
            {
                isInsecure = value;
                NotifyPropertyChanged("IsInsecure");
            }            
        }

        /// <summary>
        /// interface index
        /// </summary>
        public int InterfaceIndex
        {
            get => interfaceIndex;
            set
            {
                interfaceIndex = value;
                NotifyPropertyChanged("InterfaceIndex");
            }
        }
    
        /// <summary>
        /// interface name
        /// </summary>
        public string InterfaceName
        {
            get => interfaceName;
            set
            {
                interfaceName = value;
                NotifyPropertyChanged("InterfaceName");
            }
        }

        /// <summary>
        /// ssid
        /// </summary>
        public string SSID
        {
            get => sSID;
            set
            {
                sSID = value;
                NotifyPropertyChanged("SSID");
            }
        }

        /// <summary>
        /// Property to show WLAN error
        /// </summary>
        public bool HasNoWLAN
        {
            get => hasNoWLAN;
            set
            {
                hasNoWLAN = value;
                NotifyPropertyChanged("HasNoWLAN");
            }
        }

        /// <summary>
        /// property to show wi-fi connection error
        /// </summary>
        public bool HasNoWiFiConnection
        {
            get => hasNoWiFiConnection;
            set
            {
                hasNoWiFiConnection = value;
                NotifyPropertyChanged("HasNoWiFiConnection");
            }
        }
    #endregion
    }
}
