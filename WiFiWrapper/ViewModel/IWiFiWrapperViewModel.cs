﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WiFiWrapper.ViewModel
{
    public interface IWiFiWrapperViewModel
    {
        bool IsInsecure { get; set; }
        int InterfaceIndex { get; set; }
        string InterfaceName { get; set; }
        string SSID { get; set; }
    }
}
