﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WiFiWrapper.ViewModel;

namespace WiFiWrapper.View
{
    /// <summary>
    /// Interaction logic for WiFiWrapperView.xaml
    /// </summary>
    public partial class WiFiWrapperView : Window, IWiFiWrapperView
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="viewModel">view model of app</param>
        public WiFiWrapperView(IWiFiWrapperViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;///view model binding
        }
    }
}
