﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WiFiWrapper.View
{
    /// <summary>
    /// interface for main view
    /// </summary>
    public interface IWiFiWrapperView
    {
        void Show();///method to show window
    }
}
